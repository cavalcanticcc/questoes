
from threading import Thread

contadores = 0


def cadaThread(entrada, tamanhoLista, tamanho):
    global contadores
    usadasPalavras = []
    for i in range(tamanhoLista-tamanho+1):
        palavaAchada = entrada[i:i+tamanho]
        if(not palavaAchada in usadasPalavras):
            if(palavaAchada and entrada.find(palavaAchada, i+1) != -1):
                contadores += 1
                usadasPalavras.append(palavaAchada)


def loopAddFragmentos(entrada, tamanhoLista):
    global contadores
    processo = {}
    contagemInterna = 0
    for tamanho in range(1, tamanhoLista):
        contagemInterna += 1
        processo[tamanho] = Thread(target=cadaThread, args=[
                                   entrada, tamanhoLista, tamanho])
        processo[tamanho].start()
    for tamanho in range(1, tamanhoLista):
        processo[tamanho].join()


while True:
    entrada = input()
    if(entrada == '*'):
        break
    tamanhoLista = len(entrada)
    if(tamanhoLista == 1):
        print(0)
    else:
        loopAddFragmentos(entrada, tamanhoLista)
        print(contadores)
        contadores = 0
