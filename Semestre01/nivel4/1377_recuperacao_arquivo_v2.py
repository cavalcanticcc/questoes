
from datetime import datetime
from threading import Thread

printTime = True
contadores = 0
casos = {}
if printTime:
    casos = {
        "ababcabb": 3,
        "mississippi": 9,
        "aaaaaaaaaaaaaaaaaaaaaaaaaa": 25,
        "012345678,abcdefg.STUVWXYZ": 0,
        "say.twice,say.twice": 45,
    }
usado = 0


def cadaThread(entrada, tamanhoLista, tamanho):
    global contadores
    usadasPalavras = []
    for i in range(tamanhoLista-tamanho+1):
        palavaAchada = entrada[i:i+tamanho]
        # print(i, tamanho, palavaAchada)
        # print(usadasPalavras)
        if(not palavaAchada in usadasPalavras):
            if(palavaAchada and entrada.find(palavaAchada, i+1) != -1):
                contadores += 1
                usadasPalavras.append(palavaAchada)


def loopAddFragmentos(entrada, tamanhoLista):
    global contadores
    processo = {}
    contagemInterna = 0
    for tamanho in range(1, tamanhoLista):
        contagemInterna += 1
        processo[tamanho] = Thread(target=cadaThread, args=[
                                   entrada, tamanhoLista, tamanho])
        processo[tamanho].start()
        # cadaThread(entrada, tamanhoLista, tamanho)
    for tamanho in range(1, tamanhoLista):
        processo[tamanho].join()


while True:
    if usado >= len(casos):
        entrada = input()
        valorEsperado = ''
    else:
        entrada = list(casos)[usado]
        valorEsperado = list(casos.values())[usado]
        usado += +1
        espera = input('Usando Estudo de caso  ' + str(usado) + '   "' +
                       entrada + '"   valor esperado: ' + str(valorEsperado))
    if(entrada == '*'):
        break
    tamanhoLista = len(entrada)
    if(tamanhoLista == 1):
        print(0)
    else:
        if printTime:
            inicio = datetime.timestamp(datetime.now())
        loopAddFragmentos(entrada, tamanhoLista)
        if printTime:
            termino = datetime.timestamp(datetime.now())
            print('Tempo: ', termino - inicio)
        print(contadores)
        if printTime:
            if(valorEsperado != ''):
                print('--------Esperado-----------: '+str(valorEsperado))
        contadores = 0
