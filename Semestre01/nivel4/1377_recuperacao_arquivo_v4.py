contadores = 0

def buscarRecorrencias(entrada):
    global contadores
    tamanhoLista = len(entrada)
    palavrasAchadas = []
    for i in range(tamanhoLista-1):
        for y in range(i,tamanhoLista-1):
            if(y-i//2 >y):
                break
            palavaAchada = entrada[i:y+1]
            if((entrada.count(palavaAchada) > 1 or entrada[entrada.index(palavaAchada)+1:tamanhoLista].count(palavaAchada) == 1) and palavrasAchadas.count(palavaAchada) < 1):
                contadores += 1
                palavrasAchadas.append(palavaAchada)

while True:
    entrada = input()
    if(entrada == '*'):
        break
    if(len(entrada) == 1):
        print(0)
    else:
        buscarRecorrencias(entrada)
        print(contadores)
        contadores = 0
