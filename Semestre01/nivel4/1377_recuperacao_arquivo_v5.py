def encontre_subsequencias_repetidas(data):
    n = len(data)
    # dicionário para armazenar as subsequências e suas frequências
    subsequences = {}
    for i in range(n):
        for j in range(i+1, n+1):
            # pegando as subsequências contíguas
            subseq = data[i:j]
            if subseq in subsequences:
                subsequences[subseq] += 1
            else:
                subsequences[subseq] = 1
    # contando as subsequências que aparecem pelo menos duas vezes
    count = 0
    for key in subsequences:
        if subsequences[key] > 1:
            count += 1
    return count

data = input()
while data != "*":
    print(encontre_subsequencias_repetidas(data))
    data = input()