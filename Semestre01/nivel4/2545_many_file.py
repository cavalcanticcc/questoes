from datetime import datetime
import numpy as np
import json
from entradas_many_file import entradas

printTime = True
teste = False

listaArquivosAnalisados = []
ultimaPosicao = 1
# pend = {}
arquivosComDependencias = []
listaEmArvore = {}

limiteLoopMax = 20
testeIndex = 0
findError = False


def alinhamentoObjeto(arquivoAnalisado, dependenciasArquivo):
    global limiteLoopMax
    global listaArquivosAnalisados
    global ultimaPosicao
    # global pend
    global listaEmArvore
    global arquivosComDependencias
    print('def alinhamentoObjeto')
    if arquivoAnalisado in listaEmArvore.values():
        print('JAJAJA ja tem na lista')
    else:
        print('CRIANDO listaEmArvore ', arquivoAnalisado)
        listaEmArvore[arquivoAnalisado] = {
            'dependencias': dependenciasArquivo,
            'subgalhos': {}}
    achouEmAlgumGanho = False
    for galho in listaEmArvore:
        print('arquivoAnalisado ', arquivoAnalisado)
        print('galho ', galho)
        print('galho in dependenciasArquivo ', galho in dependenciasArquivo)
        print("'not arquivoAnalisado in listaEmArvore[galho]['dependencias'] '",
              not arquivoAnalisado in listaEmArvore[galho]['dependencias'])
        if galho in dependenciasArquivo and not arquivoAnalisado in listaEmArvore[galho]['dependencias']:
            print('ENTROU IF ', galho, listaEmArvore[galho])
            achouEmAlgumGanho = True
            listaEmArvore[arquivoAnalisado]['subgalhos'][galho] = listaEmArvore[galho].copy(
            )
        elif arquivoAnalisado in listaEmArvore[galho]['dependencias']:
            print('ENTROU ELIF FFFF', galho, listaEmArvore[galho])
            erro = True
            print('ERRRRRROOOOO')
    print('dependenciasArquivo:... ', dependenciasArquivo)


def proximaBuscaArquivo(arquivoAnalisado):
    global listaArquivosAnalisados
    global ultimaPosicao
    # global pend
    global arquivosComDependencias
    global listaEmArvore
    print('Entrando def proximaBuscaArquivo', arquivoAnalisado)
    listaEmArvoreJson = json.dumps(listaEmArvore, indent=4)
    print('listaEmArvore', listaEmArvoreJson)
    if not arquivoAnalisado in listaArquivosAnalisados:
        listaArquivosAnalisados.append(arquivoAnalisado)
        print('listaArquivosAnalisados ', listaArquivosAnalisados)
        print('arquivoAnalisado ', arquivoAnalisado)
        dependenciasArquivo = arquivosComDependencias[arquivoAnalisado].copy()
        if len(dependenciasArquivo) > 0:
            alinhamentoObjeto(arquivoAnalisado, dependenciasArquivo)
            for dependenciaArquivo in dependenciasArquivo:
                print('proximaBuscaArquivo, for: ', dependenciaArquivo)
                proximaBuscaArquivo(dependenciaArquivo)


while True:
    try:
        erro = False
        if printTime:
            inicio = datetime.timestamp(datetime.now())
        if not teste:
            entrada = int(input())
            valorEsperado = ''
        else:
            entrada = int(entradas[testeIndex])
            testeIndex += 1
        if(not entrada or entrada == None or entrada == ''):
            break
        if(entrada == '0' or entrada == 0):
            print(0)

        tempo = 1
        itensUsados = []
        contador = 0
        semDependencia = []

        for linha in range(entrada):
            temp = input()
            temp = temp.split(' ')
            # temp.pop(0)
            arquivosComDependencias.append(list(map(int, temp)))
            print('arquivosComDependencias', arquivosComDependencias)
            print('linha', linha)
            if(arquivosComDependencias[linha][0] == 0):
                semDependencia.append(linha+1)
        for linha in range(entrada):
            if (linha+1) in semDependencia:
                arquivosComDependencias[linha][0] = -1
            elif arquivosComDependencias[linha][0] != -1 and arquivosComDependencias[linha].find(linha+1,1) > -1:
                
                arquivosComDependencias[linha]    
        print('semDependencia', semDependencia)
        # for cdGrupMin in range(1, len(arquivosComDependencias)-1):
        #     print('cdGrupMin contagem: ', cdGrupMin)
        #     if not cdGrupMin in listaArquivosAnalisados:
        #         if len(arquivosComDependencias[cdGrupMin]) > 0:
        #             print('arquivosComDependencias: ', arquivosComDependencias, 'cdGrupMin: ', cdGrupMin)
        #             print('entro if ', cdGrupMin)
        #             proximaBuscaArquivo(cdGrupMin)

        # if printTime:
        #     termino = datetime.timestamp(datetime.now())
        #     # print('Pend: ', pend)
        #     print('Tempo: ', termino - inicio)
    except EOFError:
        break
