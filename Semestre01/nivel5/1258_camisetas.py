from operator import itemgetter
teste = False
testeIndex = 0
entradas = [9,
            'Maria Jose',
            'branco P',
            'Mangojata Mancuda',
            'vermelho P',
            'Cezar Torres Mo',
            'branco P',
            'Baka Lhau',
            'vermelho P',
            'JuJu Mentina',
            'branco M',
            'Amaro Dinha',
            'vermelho P',
            'Adabi Finho',
            'branco G',
            'Severina Rigudinha',
            'branco G',
            'Carlos Chade Losna',
            'vermelho P',
            3,
            'Maria Joao',
            'branco P',
            'Marcio Guess',
            'vermelho P',
            'Maria Jose',
            'branco P',
            0, ]

while True:
    if teste:
        entrada = entradas[testeIndex]
        testeIndex += 1
    else:
        entrada = int(input())
    if entrada == 0:
        break
    pessoas = []

    for usuario in range(entrada):
        if teste:
            temp = entradas[testeIndex]
            testeIndex += 1
        else:
            temp = input()
        nome = temp
        if teste:
            temp = entradas[testeIndex].split(' ')
            testeIndex += 1
        else:
            temp = input().split(' ')

        [cor, tamanho] = temp
        dados = {
            'nome': nome,
            'cor': cor,
            'tamanho': tamanho,
        }
        pessoas.append(dados)
    listaOrdenada = sorted(pessoas, key=itemgetter(
        'cor', 'tamanho', 'nome'))
    listaCores = {
        'branco': {
            'P': [],
            'M': [],
            'G': [],
        },
        'vermelho': {
            'P': [],
            'M': [],
            'G': [],
        }
    }
    for ordenada in range(len(listaOrdenada)):
        listaCores[listaOrdenada[ordenada]['cor']][listaOrdenada[ordenada]['tamanho']].append(
            listaOrdenada[ordenada])
    # print(listaCores)

    for ordem1 in listaCores:
        for ordem2 in listaCores[ordem1]:
            for lista in range(len(listaCores[ordem1][ordem2])):
                print(listaCores[ordem1][ordem2][lista]['cor'], listaCores[ordem1]
                      [ordem2][lista]['tamanho'], listaCores[ordem1][ordem2][lista]['nome'])
    print()
