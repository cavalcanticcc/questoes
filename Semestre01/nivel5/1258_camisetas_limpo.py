from operator import itemgetter

while True:
    entrada = int(input())
    if entrada == 0:
        break
    pessoas = []

    for usuario in range(entrada):
        temp = input()
        nome = temp
        temp = input().split(' ')
        [cor, tamanho] = temp
        dados = {
            'nome': nome,
            'cor': cor,
            'tamanho': tamanho,
        }
        pessoas.append(dados)
    listaOrdenada = sorted(pessoas, key=itemgetter(
        'cor', 'tamanho', 'nome'))
    listaCores = {
        'branco': {
            'P': [],
            'M': [],
            'G': [],
        },
        'vermelho': {
            'P': [],
            'M': [],
            'G': [],
        }
    }
    for ordenada in range(len(listaOrdenada)):
        listaCores[listaOrdenada[ordenada]['cor']][listaOrdenada[ordenada]['tamanho']].append(
            listaOrdenada[ordenada])

    for ordem1 in listaCores:
        for ordem2 in listaCores[ordem1]:
            for lista in range(len(listaCores[ordem1][ordem2])):
                print(listaCores[ordem1][ordem2][lista]['cor'], listaCores[ordem1]
                      [ordem2][lista]['tamanho'], listaCores[ordem1][ordem2][lista]['nome'])
    print()
