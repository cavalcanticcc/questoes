
teste = False
descricao1 = False
descricao2 = False
testeIndex = 0

entradas = [
    '4 3',
    '3 1 1',
    '5 3',
    '4 3 2',
    '5 1',
    '2',
    '5 4',
    '4 3 2 1',
    '3 1',
    '1',
    '6 5',
    '2 2 2 2 1',
    '0 0',
    '0 0',
]
saidas = [
    '4 2 3 1'
    '1 3 2 5 4',
    '5 4 3 2 1',
    '2 1 3',
    '6 5 1 2 3 4',
]
primeiroNumeroSequencia = 1

numerosUsadoLista = []


def trocaOrdemDois(a):
    global numerosUsadoLista
    if(a >= 1):
        if numerosUsadoLista[a-1] < numerosUsadoLista[a]:
            [numerosUsadoLista[a - 1], numerosUsadoLista[a]
             ] = [numerosUsadoLista[a], numerosUsadoLista[a - 1]]
        elif(a >= 0):
            trocaOrdemDois(a+1)
        else:
            print('problema negativo:', a, numerosUsadoLista)
    else:
        print('problema a < 1:', a, numerosUsadoLista)


while True:
    numerosUsadoLista = []

    if teste:
        entrada = entradas[testeIndex]
        testeIndex += 1
    else:
        entrada = input()
    [quantidade, turnos] = entrada.split(' ')
    [quantidade, turnos] = [int(quantidade), int(turnos)]
    if quantidade == 0 and turnos == 0:
        break

    if teste:
        sequencisStringInteracoes = entradas[testeIndex]
        testeIndex += 1
    else:
        sequencisStringInteracoes = input()
    sequencisInteracoes = sequencisStringInteracoes.split(' ')
    sequencisInteracoes.reverse()
    if descricao1:
        print('quantidade: ', quantidade, 'turnos: ', turnos)
    for x in range(1, quantidade+1):
        numerosUsadoLista.append(x)

    ultimaPosicao = quantidade - 1
    for turno in range(turnos):
        try:
            for interacoesNoTurno in range(int(sequencisInteracoes[turno]), 0, -1):
                if descricao1:
                    temp01 = numerosUsadoLista.copy()
                trocaOrdemDois(interacoesNoTurno)
                if descricao1:
                    print('tur: {} seq[{}]: int: {}'.format(turno, sequencisInteracoes[turno],
                                                            interacoesNoTurno), temp01, ' -> ', numerosUsadoLista)
        except:
            print("Erro: {} {} {} {} ".format(turno, interacoesNoTurno,
                  sequencisInteracoes, numerosUsadoLista))

    retorno = ''

    for achado in numerosUsadoLista:
        retorno += str(achado) + " "
    print(retorno.strip())
    if teste:
        input()

    # Erro no beecrown
#     Traceback (most recent call last):
# File "/judge/judge-b325a59e2257418ba8c14b2bcc88ea81.d/Main.py", line 76, in <module>
# trocaOrdemDois(interacoesNoTurno)
# File "/judge/judge-b325a59e2257418ba8c14b2bcc88ea81.d/Main.py", line 35, in trocaOrdemDois
# trocaOrdemDois(a+1)
# File "/judge/judge-b325a59e2257418ba8c14b2bcc88ea81.d/Main.py", line 35, in trocaOrdemDois
# trocaOrdemDois(a+1)
# File "/judge/judge-b325a59e2257418ba8c14b2bcc88ea81.d/Main.py", line 35, in trocaOrdemDois
# trocaOrdemDois(a+1)
# [Previous line repeated 995 more times]
# File "/judge/judge-b325a59e2257418ba8c14b2bcc88ea81.d/Main.py", line 30, in trocaOrdemDois
# if(a >= 1):
# RecursionError: maximum recursion depth exceeded in comparison


# https://pythonexamples.org/python-bubble-sort-program/
def bubble_sort(nlist):
    for i in range(len(nlist) - 1, 0, -1):
        no_swap = True
        for j in range(0, i):
            if nlist[j + 1] < nlist[j]:
                nlist[j], nlist[j + 1] = nlist[j + 1], nlist[j]
                no_swap = False
        if no_swap:
            return


if descricao2:
    # input list
    alist = [1, 74, 96, 5, 42, 63]
    print('Input List\n', alist)

    # sort list
    bubble_sort(alist)
    print('Sorted List\n', alist)
