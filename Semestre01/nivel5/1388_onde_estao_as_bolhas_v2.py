

teste = True
if teste:
    from entradas_1388 import entradas
    from saidas_1388 import saidas
    from datetime import datetime
descricao1 = True
descricao2 = True
printTime = True
testeIndex = 0
testeSaidaIndex = 0
numeroMovimentado = -1
posicaoNumeroMovimentado = -1
primeiroNumeroSequencia = 1
numerosMovimentados = []
numerosUsadoLista = []

while True:
    numerosUsadoLista = []
    if teste:
        entrada = entradas[testeIndex]
        testeIndex += 1
    else:
        entrada = input()
    if printTime:
        inicio = datetime.timestamp(datetime.now())
    [quantidade, turnos] = entrada.split(' ')
    [quantidade, turnos] = [int(quantidade), int(turnos)]
    if quantidade == 0 and turnos == 0:
        break

    if teste:
        sequencisStringInteracoes = entradas[testeIndex]
        testeIndex += 1
    else:
        sequencisStringInteracoes = input()
    sequencisInteracoes = sequencisStringInteracoes.split(' ')
    sequencisInteracoes.reverse()
    if descricao1:
        print('quantidade: ', quantidade, 'turnos: ', turnos)
    
    numerosUsadoLista = list(range(1, quantidade+1))

    ultimaPosicao = quantidade - 1
    if descricao1:
        print('sequencisInteracoes ', sequencisInteracoes)

    # [5, 4, 3, 2, 1]
    # 1 x 2, 1 x 3, 1 x 4 e 1 x 5;
    # 2 x 3, 2 x 4 e 2 x 5;
    # 3 x 4 e 3 x 5;
    # 4 x 5;

    # original = numerosUsadoLista.copy()
    for turno in range(turnos):
        if descricao1:
            temp01 = numerosUsadoLista.copy()
        numeroMovimentado = turnos - turno
        posicaoNumeroMovimentado = numerosUsadoLista.index(numeroMovimentado)
        if descricao1:
            print('sequencisInteracoes[turno]', sequencisInteracoes[turno])
            print('n:', numeroMovimentado, 'p:', posicaoNumeroMovimentado,
                  'quanInt:', sequencisInteracoes[turno])

        numerosUsadoLista.remove(numeroMovimentado)
        numerosUsadoLista.insert(
            int(posicaoNumeroMovimentado) +
            int(sequencisInteracoes[turno]),
            numeroMovimentado)
        if descricao1:
            print('tur: {} seq[{}]'.format(
                turno, sequencisInteracoes[turno]), temp01, ' -> ', numerosUsadoLista)
    arr = list(map(str, numerosUsadoLista))
    retorno = ' '.join(arr)
    print(retorno)
    # for achado in numerosUsadoLista:
    #     retorno += str(achado) + " "
    # retorono = retorno.strip()
    # print(retorono)
    if descricao1:
        print('Valor esperado:', saidas[testeSaidaIndex])
    if teste:
        print('Se ok:', saidas[testeSaidaIndex] == retorno)
        testeSaidaIndex += 1
    if printTime:
        termino = datetime.timestamp(datetime.now())
        print('Tempo: ', termino - inicio)
    if teste:
        input()
