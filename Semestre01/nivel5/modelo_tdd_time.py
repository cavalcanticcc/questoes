
from entradas_1388 import entradas
from saidas_1388 import saidas
from datetime import datetime

teste = True
descricao1 = True
descricao2 = False
printTime = True
testeIndex = 0
testeSaidaIndex = 0
primeiroNumeroSequencia = 1

numerosUsadoLista = []


def trocaOrdemDois(a):
    global numerosUsadoLista
    if(a >= 1):
        if numerosUsadoLista[a-1] < numerosUsadoLista[a]:
            [numerosUsadoLista[a - 1], numerosUsadoLista[a]
             ] = [numerosUsadoLista[a], numerosUsadoLista[a - 1]]
        elif(a >= 0):
            trocaOrdemDois(a+1)
        else:
            print('problema negativo:', a, numerosUsadoLista)
    else:
        print('problema a < 1:', a, numerosUsadoLista)


while True:
    numerosUsadoLista = []

    if teste:
        entrada = entradas[testeIndex]
        testeIndex += 1
    else:
        entrada = input()
    if printTime:
        inicio = datetime.timestamp(datetime.now())
    [quantidade, turnos] = entrada.split(' ')
    [quantidade, turnos] = [int(quantidade), int(turnos)]
    if quantidade == 0 and turnos == 0:
        break

    if teste:
        sequencisStringInteracoes = entradas[testeIndex]
        testeIndex += 1
    else:
        sequencisStringInteracoes = input()
    sequencisInteracoes = sequencisStringInteracoes.split(' ')
    sequencisInteracoes.reverse()
    if descricao1:
        print('quantidade: ', quantidade, 'turnos: ', turnos)
    for x in range(1, quantidade+1):
        numerosUsadoLista.append(x)

    ultimaPosicao = quantidade - 1
    for turno in range(turnos):
        try:
            for interacoesNoTurno in range(int(sequencisInteracoes[turno]), 0, -1):
                if descricao1:
                    temp01 = numerosUsadoLista.copy()
                trocaOrdemDois(interacoesNoTurno)
                if descricao1:
                    print('tur: {} seq[{}]: int: {}'.format(turno, sequencisInteracoes[turno],
                                                            interacoesNoTurno), temp01, ' -> ', numerosUsadoLista)
        except:
            print("Erro: {} {} {} {} ".format(turno, interacoesNoTurno,
                  sequencisInteracoes, numerosUsadoLista))

    retorno = ''

    for achado in numerosUsadoLista:
        retorno += str(achado) + " "
    retorono = retorno.strip()
    if teste:
        print('Resultado encontrado: ')
        
    print(retorono)
    if teste:
        print('Resultado esperado: ',saidas[testeSaidaIndex])
        print(saidas[testeSaidaIndex] == retorono)
        testeSaidaIndex += 1
    if printTime:
        termino = datetime.timestamp(datetime.now())
        print('Tempo: ', termino - inicio)
    if teste:
        input()
