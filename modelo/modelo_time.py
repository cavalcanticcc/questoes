from datetime import datetime

printTime = False

casos = {}
if printTime:
    casos = {
        "1": [
            2,'1 2','1 1'
        ],
        "2": [
            3,'0','1 3'
        ],
        "0": [
            0
        ],
    }
usado = 0

while True:
    if usado >= len(casos):
        entrada = input()
        valorEsperado = ''
    else:
        entrada = list(casos)[usado]
        valorEsperado = list(casos.values())[usado]
        usado += +1
        print('---------')
        espera = input('Usando Estudo de caso  ' + str(usado) + '   "' +
                       entrada + '"   valor esperado: ' + str(valorEsperado))
    if(entrada == '0' or entrada == None or entrada == '' ):
        break
    if printTime:
        inicio = datetime.timestamp(datetime.now())
    if printTime:
        termino = datetime.timestamp(datetime.now())
        print('Tempo: ', termino - inicio)




